ARG from
# Build libglvnd
FROM opensuse/leap:15.3 as glvnd

RUN zypper -n install \
        git \
        make \
        libtool \
        gcc \
        pkgconfig \
        python \
        libXext-devel \
        libX11-devel \
        xorg-x11-devel \
        glproto-devel \
        python-xml \
        && rm -rf /var/cache/zypp/*

ARG LIBGLVND_VERSION

WORKDIR /opt/libglvnd
RUN git clone --branch="${LIBGLVND_VERSION}" https://github.com/NVIDIA/libglvnd.git . && \
    ./autogen.sh && \
    ./configure --prefix=/usr/local --libdir=/usr/local/lib64 && \
    make -j"$(nproc)" install-strip && \
    find /usr/local/lib64 -type f -name 'lib*.la' -delete

RUN zypper -n in \
        glibc-devel \
        libgcc_s1 \
        libXext-devel \
        libX11-devel \
        && rm -rf /var/cache/zypp/*

# No need for these
# # 32-bit libraries
# RUN make distclean && \
#     ./autogen.sh && \
#     ./configure --prefix=/usr/local --libdir=/usr/local/lib --host=i386-linux-gnu "CFLAGS=-m32" "CXXFLAGS=-m32" "LDFLAGS=-m32" && \
#     make -j"$(nproc)" install-strip && \
#     find /usr/local/lib -type f -name 'lib*.la' -delete


FROM ${from}

RUN zypper -n in \
        libXau6 \
        libXdmcp6 \
        libxcb1 \
        libXext6 \
        libX11-6 \
        && rm -rf /var/cache/zypp/*

COPY --from=glvnd /usr/local/lib64 /usr/local/lib64
#COPY --from=glvnd /usr/local/lib /usr/local/lib

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
        ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
        ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics


COPY ./10_nvidia.json /usr/local/share/glvnd/egl_vendor.d/10_nvidia.json

RUN echo '/usr/local/lib64' >> /etc/ld.so.conf.d/glvnd.conf && \
    echo '/usr/local/lib' >> /etc/ld.so.conf.d/glvnd.conf && \
    ldconfig && \
    echo '/usr/local/$LIB/libGL.so.1' >> /etc/ld.so.preload && \
    echo '/usr/local/$LIB/libEGL.so.1' >> /etc/ld.so.preload
