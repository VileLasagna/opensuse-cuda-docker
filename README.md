# opensuse-cuda-docker

Docker images based on the [nVidia/CUDA images](https://hub.docker.com/r/nvidia/cuda) but built on top of [OpenSUSE](https://hub.docker.com/r/opensuse/leap)


nVidia provides a set of very useful docker images for both running and building
CUDA based applications. Sadly they only do so for Ubuntu, CentOS and Red Hat's UBI


Well, Ubuntu is garbage, I'm not a fan of CentOS because packages are too old and
never been too close to RedHat so... I took matters onto my own hands and by tweaking
and recombining the dockerfiles provided by nVidia themselves, put these images together

As a bonus, even though I myself don't really have an use for it, I've gone ahead
and made images extra to thos that nVidia provide:

nVidia has a set of CUDA images and some "[cudagl](https://hub.docker.com/r/nvidia/cudagl)"
if you want to run some openGL stuff on them. But, for example, it doesn't provide
any cuda images with opengl AND cudnn. So I just went and mashed those together as
well, since once you have the dockerfiles ready, it's easy stuff

## Note 1 - libnccl

The images are missing, in comparison to nVidia's official images, libnccl.
This is because nVidia doesn't provide OpenSUSE pacakages and, unlike with CUDNN,
which they put an easy direct dowload link.

If I find one of those, I'll add libnccl to the images, so they match more closely

## Note 2 - libglvnd

libglvnd seems to be currently at version 1.3 but I've left it at 1.2, again, for
parity
