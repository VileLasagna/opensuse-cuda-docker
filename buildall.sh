#!/bin/bash

set -e

REGISTRY=registry.gitlab.com/vilelasagna
IMAGE_NAME=opensuse-cuda-docker
OS_VERSION=leap15.3
GLVND_VERSION="v1.2.0"
CUDA_VERSION="11.4.1"

BASE_IMG="${IMAGE_NAME}:${CUDA_VERSION}-base-${OS_VERSION}"
RUNTIME_IMG="${IMAGE_NAME}:${CUDA_VERSION}-runtime-${OS_VERSION}"
DEVEL_IMG="${IMAGE_NAME}:${CUDA_VERSION}-devel-${OS_VERSION}"
RTDNN_IMG="${IMAGE_NAME}:${CUDA_VERSION}-cudnn8-runtime-${OS_VERSION}"
DEVDNN_IMG="${IMAGE_NAME}:${CUDA_VERSION}-cudnn8-devel-${OS_VERSION}"

RTGL_IMG="${IMAGE_NAME}:${CUDA_VERSION}-runtime-glvnd-${OS_VERSION}"
DEVGL_IMG="${IMAGE_NAME}:${CUDA_VERSION}-devel-glvnd-${OS_VERSION}"
RTDNNGL_IMG="${IMAGE_NAME}:${CUDA_VERSION}-cudnn8-runtime-glvnd-${OS_VERSION}"
DEVDNNGL_IMG="${IMAGE_NAME}:${CUDA_VERSION}-cudnn8-devel-glvnd-${OS_VERSION}"


ALL_IMAGES=(
            ${BASE_IMG}
            ${RUNTIME_IMG}
            ${DEVEL_IMG}
            ${RTDNN_IMG}
            ${DEVDNN_IMG}
            ${RTGL_IMG}
            ${DEVGL_IMG}
            ${RTDNNGL_IMG}
            ${DEVDNNGL_IMG}
           )

docker pull opensuse/leap:15.3

################################################################################
## Base
################################################################################

echo ""
echo "Building base image"
echo "${BASE_IMG}"
echo ""

docker build --squash --network=host \
-t ${BASE_IMG} \
-f Dockerfile.base .

docker tag ${BASE_IMG} ${REGISTRY}/${BASE_IMG}


################################################################################
## Runtime
################################################################################

echo ""
echo "Building Runtime image"
echo "${RUNTIME_IMG}"
echo ""

docker build --squash --network=host \
-t ${RUNTIME_IMG} \
--build-arg "from=${REGISTRY}/${BASE_IMG}" \
-f Dockerfile.runtime .
echo ALL_IMAGES[@]

docker tag ${RUNTIME_IMG} ${REGISTRY}/${RUNTIME_IMG}


################################################################################
## Devel
################################################################################

echo ""
echo "Building Development image"
echo "${DEVEL_IMG}"
echo ""

docker build --network=host \
-t ${DEVEL_IMG} \
--build-arg "from=${REGISTRY}/${RUNTIME_IMG}" \
-f Dockerfile.devel .

docker tag ${DEVEL_IMG} ${REGISTRY}/${DEVEL_IMG}


################################################################################
## Runtime + CUDNN7
################################################################################

echo ""
echo "Building Runtime with CUDNN7 image"
echo "${RTDNN_IMG}"
echo ""

docker build --squash --network=host \
-t ${RTDNN_IMG} \
--build-arg "from=${REGISTRY}/${RUNTIME_IMG}" \
-f Dockerfile.cudnn .

docker tag ${RTDNN_IMG} ${REGISTRY}/${RTDNN_IMG}


################################################################################
## Devel + CUDNN7
################################################################################

echo ""
echo "Building Development with CUDNN7 image"
echo "${DEVDNN_IMG}"
echo ""

docker build --network=host \
-t ${DEVDNN_IMG} \
--build-arg "from=${REGISTRY}/${DEVEL_IMG}" \
-f Dockerfile.cudnndev .

docker tag ${DEVDNN_IMG} ${REGISTRY}/${DEVDNN_IMG}


################################################################################
## Runtime + glvnd
################################################################################

echo ""
echo "Building Runtime with OpenGL image"
echo "${RTGL_IMG}"
echo ""

docker build --squash --network=host \
-t ${RTGL_IMG} \
--build-arg "from=${REGISTRY}/${RUNTIME_IMG}" \
--build-arg "LIBGLVND_VERSION=${GLVND_VERSION}" \
--cache-from "${RTGL_IMG}" \
-f Dockerfile.glvndruntime .

docker tag ${RTGL_IMG} ${REGISTRY}/${RTGL_IMG}


################################################################################
## Devel + glvnd
################################################################################

echo ""
echo "Building Development with OpenGL image"
echo "${DEVGL_IMG}"
echo ""

docker build --network=host \
-t ${DEVGL_IMG} \
--build-arg "from=${REGISTRY}/${RTGL_IMG}" \
--build-arg "LIBGLVND_VERSION=${GLVND_VERSION}" \
--cache-from "${DEVGL_IMG}" \
-f Dockerfile.glvnddevel .

docker tag ${DEVGL_IMG} ${REGISTRY}/${DEVGL_IMG}


################################################################################
## Runtime + glvnd + CUDNN7
################################################################################

echo ""
echo "Building Runtime with OpenGL and CUDNN7 image"
echo "${RTDNN_IMG}"
echo ""

docker build --squash --network=host \
-t ${RTDNNGL_IMG} \
--build-arg "from=${REGISTRY}/${RTGL_IMG}" \
-f Dockerfile.cudnn .

docker tag ${RTDNNGL_IMG} ${REGISTRY}/${RTDNNGL_IMG}


################################################################################
## Devel + glvnd + CUDNN7
################################################################################

echo ""
echo "Building Development with OpenGL and CUDNN7 image"
echo "${DEVDNNGL_IMG}"
echo ""

docker build --network=host \
-t ${DEVDNNGL_IMG} \
--build-arg "from=${REGISTRY}/${DEVGL_IMG}" \
-f Dockerfile.cudnndev .

docker tag ${DEVDNNGL_IMG} ${REGISTRY}/${DEVDNNGL_IMG}

###############################################################################
# Push all images and cleanup
###############################################################################

for IMG in ${ALL_IMAGES[@]}; do
    docker push ${REGISTRY}/${IMG}
done;



docker image prune -f
